package Test;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import Common.BaseTest;
import Common.CommonAction;
import Common.CommonFunc;
import Common.ImgProcess;
import Pages.HomePage;
import Pages.JoinPage;
import Pages.LoginPage;
import Pages.SeatPage;
import Pages.SelectPosPage;
import Pages.StartGuidePages;
import Pages.TicketDetailPage;
import Parameters.ConfigFileReader;
import Parameters.DataProviderClass;
import io.appium.java_client.MobileElement;
public class AppTest extends BaseTest {
	public Scalar min_val;
	public Scalar max_val;
	
	@Test(priority = 1, groups = {"Booking Ticket"}, dataProvider = "city name", dataProviderClass = DataProviderClass.class)
	public void SelectPosition(List<String> city_name) {
		try {
			System.out.println("SelectPosition started");
			StartGuidePages start_guide_page = new StartGuidePages(this.driver);
			start_guide_page.first_next_btn().click();
			start_guide_page.sec_next_btn().click();
			start_guide_page.third_next_btn().click();	
			SelectPosPage sele_pos_page = new SelectPosPage(this.driver);
			sele_pos_page.sele_manually_btn().click();
			
			// london click
			Thread.sleep(3000);
			sele_pos_page.city_item(city_name.get(0)).click();
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		

	}
	
	@Test(priority = 2, groups = {"Booking Ticket"}, dataProvider = "ticket name", dataProviderClass = DataProviderClass.class)
	public void SearchTicket(List<String> ticket_name) {
		try {
			System.out.println("Search Ticket started");
			// continue without signing up
			JoinPage join_page = new JoinPage(this.driver);
			join_page.continue_btn_without_sign().click();
			
			// fill search keys
			Thread.sleep(10000);
			HomePage home_page = new HomePage(this.driver);
			home_page.search_input().click();
			home_page.editable_search_field().sendKeys(ticket_name.get(0));
			Thread.sleep(5000);
			home_page.search_result_li().click();			
			
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	@Test(priority = 3, groups = {"Booking Ticket"})
	public void SelectTicket() {
		try {
			System.out.println("SelectTicket started");
			// ticket detail page
			Thread.sleep(5000);
			TicketDetailPage ticket_detail_page = new TicketDetailPage(this.driver);
			// scroll to bottom
			for(int index = 0; index < 5; index++) {
				this.common_action.scrollingToBottom();
				Thread.sleep(2000);
			}
			
			
			// get cheapest ticket
			CommonFunc common_func = new CommonFunc();
			List<MobileElement> tickets = ticket_detail_page.available_day_li();
			double min_price = common_func.extractDoubleFromStr(tickets.get(0).getText());
			MobileElement cheap_ticket = tickets.get(0);
			for(int index = 0; index < tickets.size(); index++) {
				double price = common_func.extractDoubleFromStr(tickets.get(index).getText());
				if (price < min_price) {
					cheap_ticket = tickets.get(index);
				}
			}
			cheap_ticket.click();
			ticket_detail_page.schedule_li().get(0).click();			
			
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	@Test(priority = 4, groups = {"Booking Ticket"})
	public void SelectSeat() {
		try {
			System.out.println("SelectSeat started");
			// select seat
			Thread.sleep(25000);
			
			ConfigFileReader conf_file_reader = new ConfigFileReader();
			CommonAction common_action = new CommonAction(this.driver);
			SeatPage seat_page = new SeatPage(this.driver);
						
			Rect largest_rect = seat_page.detectSeat();			
			
	        this.common_action.clickOnSpecialPos(largest_rect.x, largest_rect.y);
	        Thread.sleep(1000);
	        largest_rect = seat_page.detectSeat();
	        
	        boolean checked_seat = false;
	        int increasing_index = 0;
	        while(true) {
	        	this.common_action.clickOnSpecialPos(largest_rect.x + 20 * increasing_index, largest_rect.y + 20 * increasing_index);
	        	System.out.println("increasing_index=" + increasing_index);
	        	boolean c = true;
	        	try {
	        		seat_page.checkout_btn().click();
	        	}catch(Exception e) {
	        		increasing_index =  increasing_index + 1;
	        		c = false;
	        	}
	        	if (c) break;	        	
	        }
	        Thread.sleep(10000);
	        
			
		}catch (Exception e) {
			
		}
	}
	
	@AfterTest(alwaysRun = true)
	public void deleteDriver() {
		if (this.driver != null) {
			System.out.println("Driver will be closed.");
			this.driver.quit();
		}
	}
}
