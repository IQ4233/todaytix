package Pages;

import java.util.Iterator;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Common.CommonAction;
import Common.ImgProcess;
import Parameters.ConfigFileReader;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class SeatPage {
	AppiumDriver<MobileElement> driver;
	WebDriverWait wait;
	Scalar min_val;
	Scalar max_val;
	
	public SeatPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(this.driver, 30);
	}
	
	public MobileElement seat_map() {
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.todaytix.TodayTix:id/pyos_container")));
		return this.driver.findElementById("com.todaytix.TodayTix:id/pyos_container");
	}
	
	public MobileElement checkout_btn() {
		return this.driver.findElementById("com.todaytix.TodayTix:id/checkout_button");
	}
	
	public Rect detectSeat() throws Exception {
		ConfigFileReader conf_file_reader = new ConfigFileReader();
		CommonAction common_action = new CommonAction(this.driver);
		Point seat_map_pos = common_action.getElementLocation(this.seat_map());
		
		common_action.getScreenshot();
		ImgProcess img_proc = new ImgProcess();
		Mat img = img_proc.readImgByPath(System.getProperty("user.dir") + conf_file_reader.getScreenshotPath());
		img = img_proc.convertRGBToHSV(img);
		
		// min_val: [0, 40, 87], max_val: [50, 255, 255] : red + yellow
		// min_val: [0, 40, 87], max_val: [20, 250, 250] : red
		// min_val: [20, 40, 87], max_val: [30, 250, 250] : yellow		
		min_val = new Scalar(120, 40, 87);
		max_val = new Scalar(170, 255, 255);
		img_proc.detectObjectByColorRange(img, this.min_val, this.max_val);
		img = img_proc.removeNoise(img);
        List<MatOfPoint> contours = img_proc.findingContoursFromImage(img);
        
        Iterator<MatOfPoint> it = contours.iterator();
        while (it.hasNext()) {
           MatOfPoint i = it.next();
           Rect rect = Imgproc.boundingRect(i);
           if (rect.y < seat_map_pos.y) {
        	   it.remove();
           }
        }
        
        MatOfPoint largest_contour = img_proc.findingLargestAreaContour(contours);
        Rect largest_rect = Imgproc.boundingRect(largest_contour);
        System.out.println(largest_rect.x + ": " + largest_rect.y + ", area=" + largest_rect.width + " * " + largest_rect.height);
        return largest_rect;
	}
	
}
