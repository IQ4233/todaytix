package Pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class TicketDetailPage {
	AppiumDriver<MobileElement> driver;
	WebDriverWait wait;
	
	public TicketDetailPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(this.driver, 10);
	}
	
	public MobileElement ticket_title() {
		this.wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("com.todaytix.TodayTix:id/title_label")));
		return this.driver.findElementById("com.todaytix.TodayTix:id/title_label");
	}
	
	public List<MobileElement> available_day_li() {
		List<MobileElement> result = new ArrayList<MobileElement>();
		this.wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("com.todaytix.TodayTix:id/price")));
		List<MobileElement> day_li = this.driver.findElementsById("com.todaytix.TodayTix:id/price");
		for(int index = 0; index < day_li.size(); index++) {
			if (day_li.get(index).getText().contains("�")) {
				result.add(day_li.get(index));
			}
		}
		return result;		
	}
	
	public List<MobileElement> schedule_li() {
		this.wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("com.todaytix.TodayTix:id/performance_time")));
		return this.driver.findElementsById("com.todaytix.TodayTix:id/performance_time");
	}
}
