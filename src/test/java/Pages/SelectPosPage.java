package Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class SelectPosPage {
	AppiumDriver<MobileElement> driver;
	WebDriverWait wait;
	
	public SelectPosPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(this.driver, 10);		
	}
	
	public MobileElement enable_loc_btn() {
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.todaytix.TodayTix:id/primary_button_1")));
		return this.driver.findElementById("com.todaytix.TodayTix:id/primary_button_1");
	}
	
	public MobileElement sele_manually_btn() {
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.todaytix.TodayTix:id/secondary_button")));
		return this.driver.findElementById("com.todaytix.TodayTix:id/secondary_button");
	}
	
	public MobileElement city_item(String city_name) {
		MobileElement my_city = null;
		this.wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("com.todaytix.TodayTix:id/location_text")));
		List<MobileElement> city_li = this.driver.findElementsById("com.todaytix.TodayTix:id/location_text");
		for(int index = 0; index < city_li.size(); index++) {
			if (city_li.get(index).getText().contains(city_name)) {
				my_city = city_li.get(index);
				break;
			}
		}
		return my_city;
	}
}
