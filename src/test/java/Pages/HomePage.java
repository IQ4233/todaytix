package Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class HomePage {
	AppiumDriver<MobileElement> driver;
	WebDriverWait wait;
	
	public HomePage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(this.driver, 10);
	}
	
	public MobileElement search_input() {
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.todaytix.TodayTix:id/static_placeholder_text")));
		return this.driver.findElementById("com.todaytix.TodayTix:id/static_placeholder_text");
	}
	
	public MobileElement editable_search_field() {
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.todaytix.TodayTix:id/editable_search_field")));
		return this.driver.findElementById("com.todaytix.TodayTix:id/editable_search_field");
	}
	
	public MobileElement search_result_li() {
		this.wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("com.todaytix.TodayTix:id/poster_image_ripple")));
		return this.driver.findElementById("com.todaytix.TodayTix:id/poster_image_ripple");
	}
}
