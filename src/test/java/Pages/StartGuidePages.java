package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class StartGuidePages {
	public AppiumDriver<MobileElement> driver;
	public WebDriverWait wait;
	
	public StartGuidePages(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(this.driver, 10);
	}
	
	public MobileElement first_next_btn() {
		// id is resource id
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.todaytix.TodayTix:id/next_button")));
		return this.driver.findElementById("com.todaytix.TodayTix:id/next_button");		
	}
	
	public MobileElement sec_next_btn() {
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.todaytix.TodayTix:id/next_button")));
		return this.driver.findElementById("com.todaytix.TodayTix:id/next_button");
	}
	
	public MobileElement third_next_btn() {
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.todaytix.TodayTix:id/next_button")));
		return this.driver.findElementById("com.todaytix.TodayTix:id/next_button");
	}
}
