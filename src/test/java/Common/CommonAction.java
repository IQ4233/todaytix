package Common;

import java.io.File;
import java.time.Duration;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.WebDriverWait;

import Parameters.ConfigFileReader;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class CommonAction {
	AppiumDriver<MobileElement> driver;
	WebDriverWait wait;
	
	public CommonAction(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
	}
	
	public Point getElementLocation(MobileElement ele) {
		System.out.println(ele.getLocation());
		return ele.getLocation();
	}
	
	public void scrollingToBottom() {
		Dimension dim = this.driver.manage().window().getSize();
		int height = dim.getHeight();
		int width = dim.getWidth();
		int x = width / 2;
		TouchAction scroll_action = new TouchAction(this.driver);
		scroll_action.press(PointOption.point(x, (int)(height * 0.5)))
		.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
		.moveTo(PointOption.point(x, (int)(height * 0.25)))
		.release()
		.perform();		
	}
	
	
	public void scrollingToBottomOfPage() {
		
	}
	
	public void clickOnSpecialPos(int x_coordinate, int y_coordinate) {
		TouchAction action = new TouchAction(this.driver);
		action.press(PointOption.point(x_coordinate, y_coordinate)).release().perform();
	}
	
	public void getScreenshot() throws Exception {
		ConfigFileReader conf_file_reader = new ConfigFileReader();
		// Check if file exist or not in screenshot
		File f = new File(conf_file_reader.getScreenshotPath());
		if(f.exists() && !f.isDirectory()) { 
		    FileUtils.delete(f);
		}
		// Convert web driver object to TakeScreenshot
		TakesScreenshot scrShot =((TakesScreenshot)this.driver);
		// Call getScreenshotAs method to create image file
		File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
		// Move image file to new destination
		System.out.println(System.getProperty("user.dir"));
		File DestFile=new File(System.getProperty("user.dir") + conf_file_reader.getScreenshotPath());
		// Copy file at destination
		FileUtils.copyFile(SrcFile, DestFile);
	}
}
