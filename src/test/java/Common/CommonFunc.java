package Common;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonFunc {
	public CommonFunc() {
		
	}
	
	public double extractDoubleFromStr(String str) {
		double result = 0;
		Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(str);
        while(m.find()) {
            System.out.println(m.group());
            result = Double.parseDouble(m.group(0));
            break;
        }
        return result;
	}
}
