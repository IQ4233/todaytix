package Parameters;


import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.DataProvider;

public class DataProviderClass {
    @DataProvider(name="ticket name")
    public Object[][] getTicketName() {
        List<String> ticket_name = new ArrayList<String>();
//        ticket_name.add("Dear Evan Hansen");
        ticket_name.add("Mamma Mia");
        return new Object[][] {{ ticket_name }};
    }
    
    @DataProvider(name = "city name")
    public Object[][] getCityName() {
    	List<String> city_name = new ArrayList<String>();
    	city_name.add("London");
    	return new Object[][] {{ city_name }};
    }


}
